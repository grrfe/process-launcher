package fe.process

import java.io.OutputStream
import java.io.PrintWriter

sealed interface ProcessHook {
    interface Disposable : ProcessHook {
        fun dispose()
    }

    sealed interface LineReceiver : Disposable {
        fun receive(handler: ProcessHandler, line: String): Boolean

        interface All : LineReceiver {
            fun handle(handler: ProcessHandler, line: String)

            override fun receive(handler: ProcessHandler, line: String): Boolean {
                handle(handler, line)
                return false
            }

            override fun dispose() {}
        }

        interface Single : LineReceiver {
            override fun receive(handler: ProcessHandler, line: String): Boolean

            override fun dispose() {}
        }
    }

    abstract class LineContainsReceiver(private val string: String) : LineReceiver.Single {

        abstract fun handle(handler: ProcessHandler, line: String, index: Int)

        override fun receive(handler: ProcessHandler, line: String): Boolean {
            val index = line.indexOf(string)

            val found = index != -1
            if (found) handle(handler, line, index)
            return found
        }
    }

    abstract class RegexMatchReceiver(
        private val regex: Regex,
        private val containsString: String? = null
    ) : LineReceiver.Single {

        abstract fun handle(handler: ProcessHandler, matchResult: MatchResult)

        override fun receive(handler: ProcessHandler, line: String): Boolean {
            val contains = containsString == null || line.contains(containsString)
            val matchResult = if (contains) regex.matchEntire(line) else null

            val found = matchResult != null
            if (found) handle(handler, matchResult!!)
            return found
        }
    }

    abstract class Writer(outputStream: OutputStream) : LineReceiver.All {
        private val writer = PrintWriter(outputStream)

        override fun handle(handler: ProcessHandler, line: String) {
            writer.println(line)
            writer.flush()
        }

        override fun dispose() {
            writer.close()
        }

        class File(file: java.io.File) : Writer(file.outputStream())
    }

    fun interface StatusChanged : ProcessHook {
        fun changed(handler: ProcessHandler, oldStatus: ProcessStatus, newStatus: ProcessStatus)
    }
}

package fe.process

import java.io.PrintStream

fun launchProcess(vararg args: String, receiver: ProcessHook.LineReceiver.All): Int {
    val handler = ProcessHandler(args.toList()).withHook(receiver)
    return handler.start(ProcessStartConfig.Standalone)!!
}

fun launchProcess(vararg args: String, printTo: PrintStream = System.out, singleLine: Boolean = false): Int {
    val exitCode = launchProcess(*args, receiver = object : ProcessHook.LineReceiver.All {
        override fun handle(handler: ProcessHandler, line: String) {
            if (singleLine) printTo.print("$line\r")
            else printTo.println(line)
        }
    })

    if (singleLine) println()
    return exitCode
}

package fe.process

import java.util.*

private inline fun <reified T : ProcessHook> MutableList<ProcessHook>.send(action: (T) -> Unit) {
    getAll<T>().forEach(action)
}

private inline fun <reified T : ProcessHook> MutableList<ProcessHook>.getAll(): List<T> {
    return mapNotNull { it as? T }
}

class ProcessHandler(
    private val processBuilder: ProcessBuilder,
    private val hooks: MutableList<ProcessHook> = mutableListOf()
) : Runnable {
    constructor(args: List<String>, hooks: List<ProcessHook>) : this(
        ProcessBuilder(args).redirectErrorStream(true),
        hooks.toMutableList()
    )

    constructor(args: List<String>) : this(ProcessBuilder(args).redirectErrorStream(true))

    private lateinit var process: Process
    var status: ProcessStatus = ProcessStatus.Config
        private set(new) {
            val old = field
            field = new
            hooks.send<ProcessHook.StatusChanged> { it.changed(this, old, new) }

            if (status is ProcessStatus.Stopped) {
                hooks.getAll<ProcessHook.Disposable>().forEach { it.dispose() }
                hooks.clear()
            }
        }

    fun withHook(hook: ProcessHook): ProcessHandler {
        if (status != ProcessStatus.Config) return this

        hooks.add(hook)
        return this
    }

    fun start(config: ProcessStartConfig<*> = ProcessStartConfig.ThreadedProcess): Int? {
        if (status != ProcessStatus.Config) return null

        process = processBuilder.start()
        status = config.start(process, this)

        val exitCode = process.waitFor()
        status = ProcessStatus.Stopped(exitCode)

        return exitCode
    }

    fun stop() {
        if (status is ProcessStatus.Started) process.destroy()
    }

    override fun run() {
        val readers = hooks.getAll<ProcessHook.LineReceiver>().toMutableList()
        val scanner = Scanner(process.inputStream)
        while (scanner.hasNextLine()) {
            val line = scanner.nextLine()
            readers.removeIf { it.receive(this, line) }
        }

        scanner.close()
    }
}

package fe.process

sealed interface ProcessStartConfig<out T : ProcessStatus.Started> {
    fun start(process: Process, runnable: Runnable): T

    data object Standalone : ProcessStartConfig<ProcessStatus.Started.Standalone> {
        override fun start(process: Process, runnable: Runnable): ProcessStatus.Started.Standalone {
            runnable.run()
            return ProcessStatus.Started.Standalone(process.pid())
        }
    }

    abstract class ThreadedProcess : ProcessStartConfig<ProcessStatus.Started.ThreadedProcess> {
        companion object Default : ThreadedProcess() {
            override fun start(process: Process, runnable: Runnable): ProcessStatus.Started.ThreadedProcess {
                val thread = Thread(runnable)
                thread.start()

                return ProcessStatus.Started.ThreadedProcess(process.pid(), thread)
            }
        }
    }
}

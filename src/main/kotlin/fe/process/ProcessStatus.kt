package fe.process

sealed interface ProcessStatus {
    data object Config : ProcessStatus

    abstract class Started(val pid: Long) : ProcessStatus {
        class Standalone(pid: Long) : Started(pid)
        class ThreadedProcess(pid: Long, val thread: Thread) : Started(pid)
    }

    data class Stopped(val exitCode: Int) : ProcessStatus
}
